package org.example;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        Car coolCar = new Car("Cool Car", 25);
//        coolCar.accelerate();
        Truck bigTruck = new Truck("Big Truck", 20);
//        bigTruck.accelerate();

        Driver driver = new Driver();
        driver.driveCar(coolCar);
        driver.driveTruck(bigTruck);
    }
}