package org.example;

public class Truck {
    private final String name;
    private int speed;

    public Truck(String name, int speed){
        this.name = name;
        this.speed = speed;
    }

    public void accelerate() {
        int acceleration = 2;
        this.speed += acceleration;
        System.out.printf("%s: speed up to %d km/h%n", name, speed);
    }
}