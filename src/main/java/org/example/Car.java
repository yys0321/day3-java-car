package org.example;

public class Car {
    private final String name;
    private int speed;

    public Car(String name, int speed) {
        this.name = name;
        this.speed = speed;
    }

    public void accelerate() {
        int acceleration = 5;
        this.speed += acceleration;
//        System.out.println(name + ": speed up to " + speed + " km/h");
        System.out.printf("%s: speed up to %d km/h%n", name, speed);
    }
}