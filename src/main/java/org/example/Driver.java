package org.example;

public class Driver {
    public void driveCar(Car car) {
        car.accelerate();
    }
    public void driveTruck(Truck truck) {
        truck.accelerate();
    }
}